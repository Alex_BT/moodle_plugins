<?php
// require_once('../../course/externallib.php');
require_once('../../config.php');
require_once($CFG->dirroot .'/course/lib.php');
require_once($CFG->libdir .'/filelib.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .courses-list {
            display: grid;
            gap: 1em;
            padding: 1em 0;
        }
        .courses-list__item {
            padding: 1em;
            display: flex;
            border-bottom: 1px solid gray;
            gap: 1em;
        }
        .custom-checkbox {

        }
        .custom-checkbox__input {
            outline: none;
        }
        .course-selected {
            display: grid;
            gap: 1em;
            padding: 1em;
            border: 1px solid gray;
        }
        .course-selected__course {
            display: flex;
            justify-content: space-between;
        }
        .path-backup .backup_progress {
            display: none;
        }
        .course-selected .submit {
            margin-top: 2em;
            justify-self: flex-start;
        }
        .course-select__sections {
            padding: 1em;
            display: grid;
            gap: 0.5em;
        }
    </style>
</head>
<body>

    <div class="app">
        <section v-if="loaded">
                    
            <div v-if="!success" class="course-search">
                <input v-model="search.input" placeholder="Введите текст для поиска">
                <select v-model="search.mode">
                    <option value="course">Курс</option>
                    <option value="course_sections">Тема</option>
                    <option value="course_items">Элемент темы</option>
                </select>
            </div>





            <div v-if="!success" class="courses-list">
                <label v-for="item in results" class="courses-list__item custom-checkbox">
                    <input @change="onChose(item)" class="custom-checkbox__input" type="checkbox">
                    <div v-if="search.mode == 'course'">
                        {{ item.fullname }} ----
                        {{ new Date(item.timemodified * 1000).toLocaleString() }}
                    </div>
                    <div v-if="search.mode == 'course_sections'">
                        Курс: {{ getCourse(item.course).fullname }}
                        -- Тема: {{ item.name }} {{item.section }} ----
                        {{ new Date(item.timemodified * 1000).toLocaleString() }}
                    </div>
                    <div v-if="search.mode == 'course_items'">
                        Курс: {{ getCourse(item.course).fullname }}
                        {{ item.db_info?.name }}
                        {{ item.section }} ----
                        {{ new Date(item.db_info?.timemodified * 1000).toLocaleString() }}
                    </div>
                </label>
            </div>


            <div v-if="!success" class="course-selected">
                <div v-for="(selectItem, id) in selected" class="course-selected__item">
                    
                    <div v-if="selectItem.fullname" class="">
                        <div class="course-selected__course">
                            {{ selectItem.fullname }}
                            <span @click="selected.splice(id, 1)">удалить</span>
                        </div>
                        <div class="course-select__sections">
                            <div v-for="courseSection in getCourseSections(selectItem.id)">
                                <div> 
                                    <input :checked="selectItem.sections.findIndex(i => i == courseSection) >= 0" @change="onSectionChose(courseSection, selectItem)" class="custom-checkbox__input" type="checkbox">
                                    Тема: {{ courseSection.section }} {{ courseSection.name }} 
                                </div>
                                <div v-if="courseSection.modules" style="padding: 1em">
                                    <div v-for="courseModule in getCourseModules(selectItem.id, courseSection.id)"> 
                                        <div v-if="courseModule.db_info">
                                            <input :checked="courseSection.modules.findIndex(i => i.id == courseModule.id) >= 0 || courseSection.modules.findIndex(i => i.name == courseModule.db_info.name) >= 0" @change="onModuleChose(courseModule, selectItem, courseSection )" class="custom-checkbox__input" type="checkbox"> 
                                            Модуль: {{ courseModule.id }} {{ c_modules[courseModule.module]?.name }} {{ courseModule.db_info.name }} {{ new Date ( courseModule.db_info.timemodified * 1000 ).toLocaleString() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button v-if="selected.length" @click="submitImport" class="submit">Импортировать</button>
            </div>
            <div v-else>
                Импорт завершен!
            </div>
        </section>

    
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue@2.7.16/dist/vue.js"></script>
    
    <script>

            let app = new Vue({
                el: '.app',
                computed: {
                        results: function () {
                            const s = this.search.input.toLowerCase();
                            switch (this.search.mode) {
                                case 'course':
                                    if (s) {
                                        return Object.values(this.courses).filter(c => c.fullname.toLowerCase().indexOf(s) >= 0)
                                    } else {
                                        return this.courses
                                    }
                                    break;
                                case 'course_items':
                                    let items = []
                                    items = Object.values(this.course_modules)
                                    items.forEach(item => {
                                        item.db_info = this.getModuleInfo(item) 
                                    })
                                    if (s) {
                                        return items.filter(item => `${this.getCourse(item.course).fullname} ${item.db_info?.name} ${item.section}`.toLowerCase().indexOf(s) >= 0)
                                    } else {
                                        return items
                                    }
                                    break;
                                case 'course_sections':
                                    if (s) {
                                        return Object.values(this.course_sections).filter(item => `${this.getCourse(item.course).fullname} ${item.name}`.toLowerCase().indexOf(s) >= 0)
                                    } else {
                                        return this.course_sections
                                    }
                                    break;
                                default:
                                    return this.courses
                                    break;
                            }
                        }
                },
                mounted: async function () {
                    
                    for (const module of Object.values(this.c_modules)) {
                        let res = await fetch(`test.php?module_name=${module.name}`)
                        res = await res.json()
                        this.modules_db[module.name] = res
                    }
                    this.loaded = true
                },
                data: {
                        loaded: false,
                        selected: [],
                        success: false,
                        courses: <?= json_encode($DB->get_records('course', ['visible' => 1])) ?>,
                        c_modules: <?= json_encode($DB->get_records('modules')) ?>,
                        course_modules: <?= json_encode($DB->get_records('course_modules', ['visible' => 1, 'deletioninprogress' => 0])) ?>,
                        modules_db: {},
                        course_sections: <?= json_encode($DB->get_records('course_sections', ['visible' => 1])) ?>,
                        course_items: <?= json_encode($DB->get_records('choice')) ?>,
                        search: {  
                            mode: 'course',
                            input: ''
                        },
                },
                methods: {
                        async submitImport () {
                            this.success = true 
                            console.log(123)
                            let res = await fetch('test.php', {
                                method: 'POST',
                                body: JSON.stringify({
                                    destination_course_id: <?= $_GET['id'] ?>,
                                    clone: this.selected
                                })
                            })
                        },
                        getCourseSections(id) {
                            return Object.values(this.course_sections).filter(c => c.course == id)
                        },
                        getCourseModules(course_id, section_id) {
                            let items = []
                            items = Object.values(this.course_modules).filter(c => c.course == course_id && c.section == section_id)
                            items.forEach(item => {
                                item.db_info = this.getModuleInfo(item)
                            })
                            return items
                        },
                        getModuleInfo (modulePayload) {
                            let moduleType = this.c_modules[modulePayload.module].name
                            let typeModules = this.modules_db[moduleType]
                            console.log(modulePayload, moduleType, typeModules)
                            if (typeModules) {
                                return typeModules[modulePayload.instance]
                            } else {
                                return {}
                            }
                        },
                        onChose: function (item) {
                            if (this.search.mode == 'course') {
                                console.log('chosing course  from search', item, this.search.mode)
                                const isSelected = this.selected.findIndex(i => i == item)
                                if (isSelected >= 0) {
                                    this.selected.splice(isSelected)
                                } else {
                                    this.selected.unshift(item)
                                    this.getCourseSections(item.id).forEach(section => {
                                        this.onSectionChose(section, item)
                                    });
                                }
                            } 
                            if (this.search.mode == 'course_sections') {
                                let parentCourse = this.getCourse(item.course)
                                const isSelected = this.selected.find(i => i.id == parentCourse.id)
                                if (!isSelected) {
                                    console.log('chosing section from search', item, this.search.mode)
                                    this.selected.unshift(parentCourse)
                                    this.onSectionChose(item, parentCourse)
                                } else {
                                    this.onSectionChose(item, isSelected)
                                }
                            } 
                            if (this.search.mode == 'course_items') {
                                let parentCourse = this.getCourse(item.course)

                                let parentSection = Object.values(this.course_sections).find(c => c.id == item.section)
                                console.log('chosing module from search', parentSection)
                                
                                const isSelected = this.selected.find(i => i.id == parentCourse.id)
                                if (!isSelected) {
                                    parentCourse.sections = []
                                    this.selected.unshift(parentCourse)
                                }
                                const isSectionSelected = parentCourse.sections.find(i => i.id == parentSection.id)
                                console.log(isSectionSelected)
                                if (!isSectionSelected) {
                                    parentCourse.sections.unshift(parentSection)
                                }

                                this.onModuleChose(item, parentCourse, parentSection)

                            } 
                        },
                        onModuleChose: function (item, course, section) {
                            console.log('onModuleChose', item, course, section)
                            if (!section.modules) {
                                section.modules = [item]
                                return                            
                            }
                            const isSelected = section.modules.findIndex(i => i == item)
                            if (isSelected >= 0) {
                                section.modules.splice(isSelected)
                            } else {
                                section.modules.unshift(item)
                            }
                        },
                        onSectionChose: function (item, course) {
                            if (!course.sections) {
                                course.sections = [item]
                                return                            
                            }
                            const isSelected = course.sections.findIndex(i => i.id == item.id)
                            this.getCourseModules(course.id, item.id).forEach(module => {
                                this.onModuleChose(module, course, item)
                            });
                            if (isSelected >= 0) {
                                course.sections.splice(isSelected)
                            } else {
                                course.sections.unshift(item)
                            } 
                            console.log(course.sections)
                            
                            
                        },
                        getCourse: function (id) {
                            let res = Object.values(this.courses).find(c => c.id == id)
                            if (res) {
                                return res
                            } else {
                                return {}
                            }
                        }
                },
                
            })

    </script>
</body>
</html>

