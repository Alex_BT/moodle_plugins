<?php
function local_m_exp_extend_settings_navigation($settingsnav, $context) {

    // var_dump('m_exp nav extends', $settingsnav, $context);
    echo "<script>
        document.addEventListener('DOMContentLoaded', (event) => {
            let navItem = document.querySelector(`[data-key='coursereuse']`)
            if (navItem) {
                let newNode = navItem.cloneNode(true)

                var u = new URL(newNode.querySelector('a').href);
                var p = new URLSearchParams(u);

                newNode.querySelector('a').href = '/local/m_exp/importer.php?' + u.search.replace('?', '');
                newNode.querySelector('a').innerText = 'Множественный импорт'
                navItem.parentNode.insertBefore(newNode, navItem.nextSibling);
            } 
        })
    </script>";
    
}
