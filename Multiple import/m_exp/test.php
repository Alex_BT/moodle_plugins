
<?php
require_once('../../config.php');
require_once($CFG->dirroot .'/course/lib.php');
require_once($CFG->libdir .'/filelib.php');
require_once('../../course/externallib.php');

if (!empty($_GET['module_name'])) {
    echo json_encode($DB->get_records($_GET['module_name']));
    die();
}
$payload = json_decode(file_get_contents('php://input'), true);

$payload_data = []; 
if (!empty($payload['clone'])) { 
    foreach ($payload['clone'] as $courseSource) {
        foreach ($courseSource['sections'] as $sectionSource) {
            $secName = $sectionSource['name'];
            if (empty($payload_data[$secName])) {
                $payload_data[$secName] = [
                    'modules' => [],
                    'name' => $sectionSource['name'],
                    'section' => $sectionSource['section'],
                    'id' => $sectionSource['id'],
                    'sequence' => $sectionSource['sequence']
                ];
            }
            foreach ($sectionSource['modules'] as $moduleSource) {
                $modName = $moduleSource['db_info']['name'];
                if (empty($payload_data[$secName]['modules'][ $modName])) {
                    $payload_data[$secName]['modules'][ $modName] = $moduleSource;
                    var_dump('insert module');
                } else {
                    $exist = $payload_data[$secName]['modules'][ $modName];
                    if ($moduleSource['db_info']['timemodified'] > $exist['db_info']['timemodified']) {
                        $payload_data[$secName]['modules'][ $modName] = $moduleSource;
                        var_dump('update module');

                    }
                }
            }
        }
    }
}
if (!empty($payload_data)) {

    foreach ($payload_data as $sectionSource) {
            $existSection = $DB->get_record('course_sections', ['course' => $payload['destination_course_id'], 'section' => $sectionSource['section']]);
            $sectionId = null;
            if (!$existSection) {
                var_dump('create section', $sectionSource['name'], $sectionSource['sequence']);
                unset($sectionSource['id']);
                $sectionSource['course'] = $payload['destination_course_id'];
                $newId = $DB->insert_record('course_sections', (object) $sectionSource); 
                $existSection = $sectionSource;
                $existSection['id'] = $newId;
                $sectionId = $newId;
                var_dump('section insert res', $existSection);
            } else {
                $newSectionDB = new stdClass();
                $newSectionDB->id = $existSection->id;
                $newSectionDB->sequence = $sectionSource['sequence'];
                $res = $DB->update_record('course_sections', $newSectionDB); 
                $sectionId = $newSectionDB->id;
                var_dump($newSectionDB);
            }

        foreach ($sectionSource['modules'] as $moduleSource) {
            var_dump('create module', $moduleSource);
            $moduleSource['course'] = $payload['destination_course_id'];

            $moduleSource['section'] = $sectionId;
            try {
                $res = $DB->insert_record('course_modules', (object) $moduleSource); 
                var_dump('module insert res', $res);
            } catch (\Throwable $th) {
                var_dump($th);
            }
        }
        
    }
    $resMeg = course_integrity_check($payload['destination_course_id'], null, null, true);
    var_dump(1231231, $resMeg);
    rebuild_course_cache( $payload['destination_course_id']);
    die();
}

?>
