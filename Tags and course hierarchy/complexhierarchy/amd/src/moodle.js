import {call as fetchMany} from 'core/ajax';

export default {
  fetchMany,
};
