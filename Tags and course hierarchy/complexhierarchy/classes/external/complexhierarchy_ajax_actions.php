<?php
namespace format_complexhierarchy\external;

use core_external\external_function_parameters;
use core_external\external_multiple_structure;
use core_external\external_single_structure;
use core_external\external_value;
use stdClass;

class complexhierarchy_ajax_actions extends \core_external\external_api {

    public static function execute_parameters() {
        return new external_function_parameters(
            [
                'action' => new external_value(
                    PARAM_ALPHANUMEXT,
                    'action: cm_hide, cm_show, section_hide, section_show, cm_moveleft...',
                    VALUE_REQUIRED
                ),
                'data' => new external_value(
                    PARAM_RAW,
                    'data object',
                    VALUE_REQUIRED
                )
            ]
        );
    }

    public static function execute(string $action, string $data)
    {
        global $CFG, $DB, $USER;

        $response = ['ok' => true];

        if ($action === 'testing') {
            $data = json_decode($data);
            $response['data'] = $data->some;
        } else if ($action === 'StoreHierarchyData') {
            $data = json_decode($data);

            $newRecord = new stdClass();
            $newRecord->section_child_id = $data->child;
            $newRecord->section_parent_id = $data->parent;
            $newRecord->course_id = $data->course;

            // remove child parent relations

            $DB->delete_records(
                "complexhierarchy__parent_child_link",
                ['section_child_id' => $newRecord->section_child_id]
            );

            // if parent is set, store it
            // TODO: add some checks

            if ($newRecord->section_parent_id != 0)
            {
                try {
                    $DB->insert_record('complexhierarchy__parent_child_link', $newRecord, false);
                } catch (\Exception $e) {
                    // NOTE: ignore moodle fetch id exceptions
                }
            }
        } else if ($action === 'AddTagToEntity') {
            $data = json_decode($data);

            $obj = new \stdClass();
            $obj->user_id = $USER->id;
            $obj->entity_type = $data->tag_entity_type;
            $obj->entity_id = $data->tag_entity_id;
            $obj->name = $data->tag_name;

            if (isset($data->tag_is_global)) {
                $obj->user_id = 0;
            }

            $inDb = $DB->get_record('complexhierarchy__custom_tag', ['name' => $obj->name, 'user_id' => $obj->user_id, 'entity_type' => $obj->entity_type, 'entity_id' => $obj->entity_id]);

            if ($inDb !== false) {
                $id = $inDb->id;
            } else {
                $id = $DB->insert_record('complexhierarchy__custom_tag', $obj);   
            }

            $response['tag'] = [
                'id' => $id,
                'name' => $obj->name
            ];
        } else if ($action === 'RemoveTag') {
            $data = json_decode($data);
            
            $DB->delete_records(
                "complexhierarchy__parent_child_link",
                ['id' => $data->id]
            );
        } else if ($action === 'SearchTags') {
            $query = json_decode($data)->query;

            if (strlen($query) > 0) {

                $records = $DB->get_records_sql("
SELECT * FROM {complexhierarchy__custom_tag} WHERE name LIKE :query AND (user_id = 0 OR user_id = :user_id)
", ['query' => '%' . $query . '%', 'user_id' => $USER->id]);

                $resultTagsDict = [];
                foreach ($records as $rec) {
                    if (!array_key_exists($rec->name, $resultTagsDict)) {
                        $resultTagsDict[$rec->name] = [];
                    }
                    $resultTagsDict[$rec->name][] = $rec->id;
                }

                $response['tags'] = [];
                foreach ($resultTagsDict as $name => $ids) {
                    $response['tags'][] = ['id' => implode(',', $ids), 'name' => $name];
                }
            } else {
                $response['tags'] = [];
            }
        } else if ($action === 'SearchEntities') {
            $data = json_decode($data);

            $tagRecords = [];
            if (isset($data->tagIds)) {
                $tagId = $data->tagIds;

                $response['debug']= "select * from {complexhierarchy__custom_tag} where id IN ($tagId)";

                if ($tagId) {
                    $tagRecords = $DB->get_records_sql("select * from {complexhierarchy__custom_tag} where id IN ($tagId)");
                }
            } else {
                $tagQuery = $data->tagQuery;

                if (strlen($data->tagQuery)) {
                    $tagQuery = preg_replace('/\$\{([^}{]+|(?R))*+\}/', "name='$1'", $tagQuery);

                    if (strlen($tagQuery) > 0 ) {
                        $tagRecords = $DB->get_records_sql("select * from {complexhierarchy__custom_tag} where $tagQuery");
                    }
                }
            }

            // tag query to sql query
            
            if (count($tagRecords) > 0) {
                $response['entities'] = [];

                $courseIds = [];
                $sectionIds = [];
                $moduleIds = [];

                foreach ($tagRecords as $rec) {
                    if ($rec->entity_type === 'course') {
                        $courseIds[] = $rec->entity_id;
                    } else if ($rec->entity_type === 'course_section') {
                        $sectionIds[] = $rec->entity_id;
                    } else if ($rec->entity_type === 'module') {
                        $moduleIds[] = $rec->entity_id;
                    }
                }

                if (count($courseIds) > 0) {
                    $ids = implode(',', $courseIds);
                    $records = $DB->get_records_sql("select * from {course} where id in ($ids)");

                    foreach ($records as $rec) {
                        $response['entities'][] = [
                            'type' => 'course',
                            'id' => $rec->id,
                            'name' => $rec->fullname
                        ];
                    }
                }

                if (count($sectionIds) > 0) {
                    $ids = implode(',', $sectionIds);
                    $records = $DB->get_records_sql("select * from {course_sections} where id in ($ids)");

                    foreach ($records as $rec) {
                        $response['entities'][] = [
                            'type' => 'course_section',
                            'id' => $rec->id,
                            'course_id' => $rec->course,
                            'name' => $rec->name
                        ];
                    }
                }

                if (count($moduleIds) > 0) {
                    $ids = implode(',', $moduleIds);
                    $records = $DB->get_records_sql("select cm.*, m.name as module_table from {course_modules} as cm left join {modules} m on cm.module=m.id where cm.id in ($ids)");

                    foreach ($records as $rec) {
                        // $response['debug_data'][] = $rec->module_table;
                        $moduleData = $DB->get_record($rec->module_table, ['id' => $rec->instance]);
                        $response['entities'][] = [
                            'type' => 'module',
                            'module_table' => $rec->module_table,
                            'id' => $rec->id,
                            'name' => $moduleData->name
                        ];
                    }
                }
            } else {
                $response['entities'] = [];
            }
        }
        else
        {
            $response['error'] = 'Action not found: ' . $action;
        }

        return json_encode($response);
    }

    public static function execute_returns(): external_value {
        return new external_value(PARAM_RAW, 'Encoded JSON');
    }
}
