<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace format_complexhierarchy\output\courseformat;

use core_courseformat\output\local\content as content_base;

use format_complexhierarchy\output\courseformat\tag_display;

/**
 * Complex hierarchy course format content class.
 *
 * @package     format_complexhierarchy
 * @copyright   2024 Your Name <you@example.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class content extends content_base {

    /**
     * Returns the output class template path.
     *
     * This method redirects the default template when the course content is rendered.
     */
    public function get_template_name(\renderer_base $renderer): string {
        return 'format_complexhierarchy/local/content';
    }

    public function export_for_template(\renderer_base $output)
    {
        $data = parent::export_for_template($output);

        $data->courseId = $this->format->get_courseid();
        $data->isEditing = $this->format->show_editor();

        $display = new tag_display($this->format);
        $display->setForEntity('course', $data->courseId, 'Теги курса');
        $data->tag = $display->export_for_template($output);

        return $data;
    }
}
