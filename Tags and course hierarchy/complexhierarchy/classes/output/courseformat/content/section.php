<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace format_complexhierarchy\output\courseformat\content;

use core_courseformat\output\local\content\section as section_base;
use format_complexhierarchy\output\courseformat\tag_display;

/**
 * Complex hierarchy course format content class.
 *
 * @package     format_complexhierarchy
 * @copyright   2024 Your Name <you@example.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class section extends section_base {

    private $isChildBeeingRendered = false;
    
    /**
     * Returns the output class template path.
     *
     * This method redirects the default template when the course content is rendered.
     */
    public function get_template_name(\renderer_base $renderer): string {
        return 'format_complexhierarchy/local/content/section';
    }

    private $idToNumMap = null;

    public function export_for_template(\renderer_base $output) : \stdClass
    {
        if ($this->idToNumMap === null) {
            $this->idToNumMap = [];
            foreach ($this->format->get_sections() as $num => $sec) {
                $this->idToNumMap[$sec->id] = $num;
            }
        }
        
        $data = parent::export_for_template($output);

        $courseId = $this->format->get_courseid();
        //die(print_r(get_class_methods($this->format)));

        $sectionId = $data->id;

        $parentId = $this->getParentIdWithCache($sectionId, $courseId);
        if ($parentId !== null) {
            $data->parentId = $parentId;
            $data->parentName = $this->format->get_section_name($parentId);
        } else {
            $data->parentId = 0;
            $data->parentName = '';
        }

        $data->renderedChildren = [];
        $childrenIds = $this->getOrderedChildrenWithCache($sectionId, $courseId);
        if (count($childrenIds) > 0) {
            foreach ($childrenIds as $childSectionId) {
                $secNum = $this->idToNumMap[$childSectionId];
                $sectionObj = $this->format->get_section($secNum);
                $childSection = new section($this->format, $sectionObj);
                $childSection->isChildBeeingRendered = true;
                $data->renderedChildren[] = $childSection->export_for_template($output);
            }
        }
        
        $data->isFullSectionRendered =
            $this->format->show_editor()
            || $this->isChildBeeingRendered
            || $data->parentId == 0;

        if ($data->isFullSectionRendered) {
            $display = new tag_display($this->format);
            $display->setForEntity('course_section', $sectionId, 'Теги секции');
            $data->tag = $display->export_for_template($output);
        }

        return $data;
    }

    private $parentIdCache = null;
    private $allChildrenIdCache = null;
    private $directChildrenCache = null;
    
    public function getOrderedChildrenWithCache($parentId, $courseId)
    {
        global $DB;

        if ($this->directChildrenCache === null) {
            $this->buildHierarchyCache($courseId);
        }

        if (array_key_exists($parentId, $this->directChildrenCache)) {
            return $this->directChildrenCache[$parentId];
        }
        return [];
    }

    
    public function getParentIdWithCache($sectionId, $courseId)
    {
        if ($this->parentIdCache === null) {
            $this->buildHierarchyCache($courseId);
        }

        if (array_key_exists($sectionId, $this->parentIdCache)) {
            return $this->parentIdCache[$sectionId];
        }
        return null;
    }

    private function buildHierarchyCache($courseId)
    {
        global $DB;
        $records = $DB->get_records(
            'complexhierarchy__parent_child_link',
            ['course_id' => $courseId]
        );

        $this->parentIdCache = [];
        $this->directChildrenCache = [];
        $this->allChildrenIdCache = []; // TODO:
        foreach ($records as $item) {
            $this->parentIdCache[$item->section_child_id] = $item->section_parent_id;
            
            if (!array_key_exists($item->section_parent_id, $this->directChildrenCache)) {
                $this->directChildrenCache[$item->section_parent_id] = [];
            }
            $this->directChildrenCache[$item->section_parent_id][] = $item->section_child_id;
        }

        
    }
    
    public function is_section_toplevel()
    {
        return 'label from section';
        return $this->format->get_sectionid();
    }
}
