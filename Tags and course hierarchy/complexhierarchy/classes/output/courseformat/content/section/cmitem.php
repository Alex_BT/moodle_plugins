<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace format_complexhierarchy\output\courseformat\content\section;

use core_courseformat\output\local\content\section\cmitem as cmitem_base;
use renderer_base;
use stdClass;

use format_complexhierarchy\output\courseformat\tag_display;

/**
 * Complex hierarchy course format content class.
 *
 * @package     format_complexhierarchy
 * @copyright   2024 Your Name <you@example.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cmitem extends cmitem_base {

    /**
     * Returns the output class template path.
     *
     * This method redirects the default template when the course content is rendered.
     */
    public function get_template_name(\renderer_base $renderer): string {
        return 'format_complexhierarchy/local/content/section/cmitem';
    }

    /**
     * Example of override export for template data.
     *
     * @param renderer_base $output typically, the renderer that's calling this function
     * @return stdClass data context for a mustache template
     */
    public function export_for_template(renderer_base $output): stdClass {
        $format = $this->format;
        $data = parent::export_for_template($output);

        $display = new tag_display($this->format);
        $display->setForEntity('module', $data->id, 'Теги модуля');
        $data->tag = $display->export_for_template($output);
        
        return $data;
    }
}
