<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace format_complexhierarchy\output\courseformat;

use core_courseformat\output\local\content as content_base;

/**
 * Complex hierarchy course format content class.
 *
 * @package     format_complexhierarchy
 * @copyright   2024 Your Name <you@example.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class tag_display extends content_base {
    private $entityId;
    private $entityType;
    private $label;
    
    public function setForEntity($entityType, $entityId, $label)
    {
        $this->entityType = $entityType;
        $this->entityId = $entityId;
        $this->label = $label;
    }
    
    /**
     * Returns the output class template path.
     *
     * This method redirects the default template when the course content is rendered.
     */
    public function get_template_name(\renderer_base $renderer): string {
        return 'format_complexhierarchy/local/tag_display';
    }

    public function export_for_template(\renderer_base $output)
    {
        global $USER, $DB;

        $data = new \stdClass();

        $data->entity_type = $this->entityType;
        $data->entity_id = $this->entityId;
        $data->user_id = $USER->id;

        $tags = $DB->get_records('complexhierarchy__custom_tag', ['entity_type' => $this->entityType, 'entity_id' => $this->entityId]);

        $data->label = $this->label;

        $data->tags = [];
        foreach ($tags as $tag) {
            $data->tags[] = [
                'id' => $tag->id,
                'name' => $tag->name,
                'css_class' => $tag->user_id == 0 ? 'entity-tags__tag--global' : ''
            ];
        }

        return $data;
    }
}
