<?php
$functions = [
    'complexhierarchy_ajax_actions' => [
        'classname'   => 'format_complexhierarchy\external\complexhierarchy_ajax_actions',
        'description' => 'All required ajax actions in one class',
        'type'        => 'write',
        'ajax'        => true,
    ],
];
