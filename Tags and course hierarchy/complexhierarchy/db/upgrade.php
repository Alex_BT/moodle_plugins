<?php

function xmldb_format_complexhierarchy_upgrade($oldversion): bool {
    global $CFG, $DB;

    $dbman = $DB->get_manager(); // Loads ddl manager and xmldb classes.

    $v = 20240509004;
    if ($oldversion < $v) {
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // format_complex_hierarchy
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        
        // Define table format_complex_hierarchy to be created.
        $table = new xmldb_table('format_complexhierarchy');

        // Adding fields to table format_complex_hierarchy.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);

        // Adding keys to table format_complex_hierarchy.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);

        // Conditionally launch create table for format_complex_hierarchy.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Complex_hierarchy savepoint reached.
        upgrade_plugin_savepoint(true, $v, 'format', 'complexhierarchy');
    }

    $v = 20240511006;
    if ($oldversion < $v) {

        // Define table complexhierarchy__parent_child_link to be created.
        $table = new xmldb_table('complexhierarchy__parent_child_link');

        // Adding fields to table complexhierarchy__parent_child_link.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('section_parent_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('section_child_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('course_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table complexhierarchy__parent_child_link.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);

        // Conditionally launch create table for complexhierarchy__parent_child_link.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Complexhierarchy savepoint reached.
        upgrade_plugin_savepoint(true, $v, 'format', 'complexhierarchy');
    }

    $v = 20240511007;
    if ($oldversion < $v) {

        // Define table complexhierarchy__custom_tag to be created.
        $table = new xmldb_table('complexhierarchy__custom_tag');

        // Adding fields to table complexhierarchy__custom_tag.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('user_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('entity_type', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('entity_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '250', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table complexhierarchy__custom_tag.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);

        // Conditionally launch create table for complexhierarchy__custom_tag.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Complexhierarchy savepoint reached.
        upgrade_plugin_savepoint(true, $v, 'format', 'complexhierarchy');
    }

    return true;
}
