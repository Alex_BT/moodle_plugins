globalThis.HierarchyCourseEditor = (function(module){

  module.Init = function(moodle, data)
  {
    module.M = moodle;
    module.sections = data.sections;
    module.InitEventListeners();
  };

  module.InitEventListeners = function()
  {
    document.addEventListener('click', function(e) {
      if (e.target.dataset.action) {
        if (e.target.dataset.action == 'OpenParentEditor') {
          const sectionId = parseInt(e.target.dataset.section);
          const courseId = parseInt(e.target.dataset.course);
          const parentId = parseInt(e.target.dataset.parent);

          if (isNaN(parentId)) {
            parentId = 0;
          }
          
          
          let input = document.createElement('select');

          var el = document.createElement('option');
          el.value = '';
          el.innerText = 'Не выбрано';
          if (parentId == 0) {
            el.setAttribute('selected', 'selected');
          }
          input.appendChild(el);
          
          for (const itemId in module.sections) {
            el = document.createElement('option');
            el.value = itemId;
            el.innerText = module.sections[itemId];

            if (itemId == parentId) {
              el.setAttribute('selected', 'selected');
            }
            
            input.appendChild(el);
          }

          el = e.target;

          el.querySelector('.view').style.display = 'none';
          el.querySelector('.edit').style.display = 'initial';

          el.querySelector('.edit').innerHTML = '';
          el.querySelector('.edit').appendChild(input);

          input.addEventListener('change', function() {
            let sel = input.querySelector('option:checked');
            if(sel) {
              el.querySelector('.view').style.display = 'initial';
              el.querySelector('.edit').style.display = 'none';

              el.querySelector('.view .name').innerText = sel.innerText;

              const parentId = parseInt(sel.value);

              module.StoreHierarchyData(sectionId, parentId, courseId);
            }
          });
        }
      }
    });
  };

  module.Test = function()
  {
    let promises = module.M.fetchMany([{
      methodname: 'complexhierarchy_ajax_actions',
      args: {
        action: 'testing',
        data: JSON.stringify({ some: 'random stirng to echo'})
      }
    }]);
    promises[0].then(response => {console.log(response);}).catch(response => console.log(response));
  };

  module.StoreHierarchyData = function(child, parent, course)
  {
    let promises = module.M.fetchMany([{
      methodname: 'complexhierarchy_ajax_actions',
      args: {
        action: 'StoreHierarchyData',
        data: JSON.stringify({ child, parent, course })
      }
    }]);
    promises[0].then(r => {
      console.log(r);
    }).catch(r => console.error(r));
  };
  
  return module;
})(globalThis.HierarchyCourseEditor || {});


