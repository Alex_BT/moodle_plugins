globalThis.TagEditor = ((module) => {

  module.Init = (moodle) =>
  {
    module.M = moodle;
    module.InitEventListeners();
  };

  module.InitEventListeners = () => 
  {
    document.addEventListener('click', module.GlobalDocumentActionEventClickListener);
  };

  module.GlobalDocumentActionEventClickListener = (e) => {
    var actionButton = e.target.closest('[data-action=AddTagToEntity]'); 
    if (actionButton) {
      e.stopPropagation();
      e.preventDefault();

      const formData = Object.fromEntries(new FormData(actionButton.closest('form')));
      var btn = actionButton;
      
      module.SaveTagOnServer(formData).then(responses => {
        const response = JSON.parse(responses[0]);
        const tag = response.tag;

        let entityBlock = btn.closest('.entity-tags__block');

        let cloud = entityBlock.querySelector('.entity-tags__tags-cloud');

        if (!cloud.querySelector(`span[data-id="${tag.id}]"`)) {
          const li = document.createElement('li');
          li.classList.add('entity-tags__tag');
          if (formData.tag_is_global !== undefined) {
            li.classList.add('entity-tags__tag--global');
          }
          li.innerHTML = `${tag.name} <span data-action="RemoveTag" class="remove-tag-button entity-tags__buttons--edit" data-id="${tag.id}" title="Удалить тег"><i class="fa fa-times"></i></span>`;
          cloud.appendChild(li);
        }
        
        entityBlock.classList.remove('entity-tags--add-new');
        entityBlock.classList.add('entity-tags--edit');
      });
    }

    actionButton = e.target.closest('[data-action=StartEditingTags]');
    if (actionButton) {
      e.stopPropagation();
      e.preventDefault();
      
      let entityBlock = actionButton.closest('.entity-tags__block');

      entityBlock.classList.remove('entity-tags--view');
      entityBlock.classList.remove('entity-tags--add-new');
      entityBlock.classList.add('entity-tags--edit');
    }

    actionButton = e.target.closest('[data-action=OpenAddingTag]');
    if (actionButton) {
      e.stopPropagation();
      e.preventDefault();

      let entityBlock = actionButton.closest('.entity-tags__block');

      entityBlock.querySelector('input[name=tag_name]').value = '';
      entityBlock.querySelector('input[name=tag_is_global]').checked = undefined;

      entityBlock.classList.remove('entity-tags--edit');
      entityBlock.classList.add('entity-tags--add-new');
    }

    actionButton = e.target.closest('[data-action=CloseEditingTags]');
    if (actionButton) {
      e.stopPropagation();
      e.preventDefault();
      
      let entityBlock = actionButton.closest('.entity-tags__block');

      entityBlock.classList.remove('entity-tags--edit');
      entityBlock.classList.remove('entity-tags--add-new');
      entityBlock.classList.add('entity-tags--view');
      
    }

    actionButton = e.target.closest('[data-action=CloseAddingTag]');
    if (actionButton) {
      e.stopPropagation();
      e.preventDefault();
      
      let entityBlock = actionButton.closest('.entity-tags__block');

      entityBlock.classList.add('entity-tags--edit');
      entityBlock.classList.remove('entity-tags--add-new');
    }

    actionButton = e.target.closest('[data-action=RemoveTag]');
    if (actionButton) {
      e.stopPropagation();
      e.preventDefault();
      
      let entityBlock = actionButton.closest('.entity-tags__tag');
      
      module.RemoveTagOnServer(actionButton.dataset.id).then(x => {
        actionButton.closest('.entity-tags__tag').remove();
      });
    }
  };

  module.SaveTagOnServer = (formData) => {
    let promises = module.M.fetchMany([{
      methodname: 'complexhierarchy_ajax_actions',
      args: {
        action: 'AddTagToEntity',
        data: JSON.stringify(formData)
      }
    }]);
    return Promise.all(promises);
  };

  module.RemoveTagOnServer = (tagId) => {
    let promises = module.M.fetchMany([{
      methodname: 'complexhierarchy_ajax_actions',
      args: {
        action: 'RemoveTag',
        data: JSON.stringify({id: tagId})
      }
    }]);
    return Promise.all(promises);
  };
  
  return module;
})(globalThis.TagEditor || {});
