<?php

function local_custompages_extend_navigation_frontpage(\navigation_node $frontpage)
{
    global $PAGE, $CFG;
    $url = new \moodle_url($CFG->wwwroot."/local/custompages/tagsearch.php");
    $frontpage->add('Поиск по тегам', $url);
}
