globalThis.TagSearch = ((module) => {

  module.Init = function (moodle, pageContainer)
  {
    module.M = moodle;

    module.els = {
      
    };

    module.InitEventListeners();
    
  };

  module.InitEventListeners = function()
  {
    const input = document.getElementById('TagSearchInput');

    input.addEventListener('input', () => {
      const val = input.value;

      var currentPosition = input.selectionStart;
      var currentHashIndex = Number.MAX_VALUE;

      let idx = -1;
      while (idx < val.length) {
        const prevIdx = idx;
        idx = input.value.indexOf('${', prevIdx);
        if (idx === -1 || idx > currentPosition) {
          if (prevIdx !== -1) {
            currentHashIndex = prevIdx;
          }
          break;
        } else {
          idx++;
        }
      }

      if (currentPosition > currentHashIndex) {
        const query = input.value.substring(currentHashIndex+1, currentPosition);
        
        module.SearchTagsOnServer(query).then((tags) => {
          const lis = tags.map(x => {
            return `<li class="list-group-item" data-action="SearchByTag">${x.name}</li>`;
          });

          var ul = document.getElementById('TagSearchResult');
          ul.innerHTML = lis.join('');

          ul.dataset.hash_idx = currentHashIndex;
          ul.dataset.current_pos = currentPosition;
        });
      } else {
        console.log(currentPosition, currentHashIndex);
      }
    });
    
    document.body.addEventListener('click', function(e) {
      let btn = e.target.closest('[data-action=SearchByTag]');
      if (btn) {
        var ul = document.getElementById('TagSearchResult');
        var val = input.value;

        input.value = val.substring(0, ul.dataset.hash_idx-1) + '${' + btn.innerText + '}' + val.substring(ul.dataset.current_pos);

        document.getElementById('TagSearchResult').innerHTML = '';
      }

      btn = e.target.closest('[data-action=SearchEntities]');
      if (btn) {
        if (input.value.length) {
          module.SearchEntitiesByTagOnServer(input.value)
            .then(module.RenderFoundEntities);
        }
      }
    });
  };

  module.SearchTagsOnServer = function(searchString)
  {
    return Promise.all(module.M.fetchMany([{
        methodname: 'complexhierarchy_ajax_actions',
        args: {
          action: 'SearchTags',
          data: JSON.stringify({query: searchString})
        }
      }])).then((responses) => {
        var response = responses[0];
        response = JSON.parse(response);

        if (response.ok) {
          return response.tags;
        }
        return [];
      });
  };

  module.SearchEntitiesByTagOnServer = function(tagId)
  {
    return Promise.all(module.M.fetchMany([{
        methodname: 'complexhierarchy_ajax_actions',
        args: {
          action: 'SearchEntities',
          data: JSON.stringify({tagQuery: tagId})
        }
      }])).then((responses) => {
        var response = responses[0];
        response = JSON.parse(response);

        if (response.ok) {
          return response.entities;
        }
        
        return [];
      });
  };

  module.RenderFoundEntities = function(entities)
  {
    const el = document.getElementById('EntitySearchResult');
    el.innerHTML = '';
    
    if (entities.length !== undefined && entities.length > 0) {

      const lis = entities.map(e => `<li class="list-group-item">
  <a href=${module.genurl(e)} target="_blank">${e.name}</a> - <em>${module.gendescr(e)}</em>
  
</li>`);
      el.innerHTML = lis.join('');
    }
  };

  module.gendescr = function(e)
  {
    if (e.type == 'course') {
      return 'Курс';
    } else if (e.type == 'course_section') {
      return 'Секция';
    } else if (e.type == 'module') {
      return 'Модуль';
    }
    
    return '';
  };

  module.genurl = function(e)
  {
    
    if (e.type == 'course') {
      return '/course/view.php?id=' + e.id;
    } else if (e.type == 'course_section') {
      return '/course/section.php?id=' + e.id;
    } else if (e.type == 'module') {
      return '/mod/data/view.php?id=' + e.id;
    }
    
    return '';
  };

  return module;
})(globalThis.TagSearch || {});
