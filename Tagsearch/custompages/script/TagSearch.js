globalThis.TagSearch = ((module) => {

  module.Init = function (moodle, pageContainer)
  {
    module.M = moodle;

    module.els = {
      
    };

    module.InitEventListeners();
    
  };

  module.InitEventListeners = function()
  {
    const input = document.getElementById('TagSearchInput');

    input.addEventListener('input', () => {
      const val = input.value;

      module.SearchTagsOnServer(val).then((tags) => {
        const lis = tags.map(x => {
            return `<li class="list-group-item" data-action="SearchByTag" data-id="${x.id}">${x.name}</li>`;
          });

          document.getElementById('TagSearchResult').innerHTML = lis.join('');
      });
    });
    
    document.body.addEventListener('click', function(e) {
      const btn = e.target.closest('[data-action=SearchByTag]');
      if (btn) {
        var currentPosition = input.selectionStart;

        document.getElementById('TagSearchResult').innerHTML = '';

        document.getElementById('TagSearchInput').value = btn.innerText;

        module.SearchEntitiesByTagOnServer(btn.dataset.id).then(module.RenderFoundEntities);
      }
    });
  };

  module.SearchTagsOnServer = function(searchString)
  {
    return Promise.all(module.M.fetchMany([{
        methodname: 'complexhierarchy_ajax_actions',
        args: {
          action: 'SearchTags',
          data: JSON.stringify({query: searchString})
        }
      }])).then((responses) => {
        var response = responses[0];
        response = JSON.parse(response);

        if (response.ok)
        {
          return response.tags;
          
          // const lis = .map(x => {
          //   return `<li class="list-group-item" data-action="SearchByTag" data-id="${x.id}">${x.name}</li>`;
          // });

          // document.getElementById('TagSearchResult').innerHTML = lis.join('');
        }
        return [];
      });
  };

  module.SearchEntitiesByTagOnServer = function(tagId)
  {
    return Promise.all(module.M.fetchMany([{
        methodname: 'complexhierarchy_ajax_actions',
        args: {
          action: 'SearchEntities',
          data: JSON.stringify({tagIds: tagId})
        }
      }])).then((responses) => {
        var response = responses[0];
        response = JSON.parse(response);

        if (response.ok) {
          return response.entities;
        }
        
        return [];
      });
  };

  module.RenderFoundEntities = function(entities)
  {
    const el = document.getElementById('EntitySearchResult');
    el.innerHTML = '';
    
    if (entities.length !== undefined && entities.length > 0) {

      const lis = entities.map(e => `<li class="list-group-item">
  <a href=${module.genurl(e)} target="_blank">${e.name}</a> - <em>${module.gendescr(e)}</em>
  
</li>`);
      el.innerHTML = lis.join('');
    }
  };

  module.gendescr = function(e)
  {
    if (e.type == 'course') {
      return 'Курс';
    } else if (e.type == 'course_section') {
      return 'Секция';
    } else if (e.type == 'module') {
      return 'Модуль';
    }
    
    return '';
  };

  module.genurl = function(e)
  {
    
    if (e.type == 'course') {
      return '/course/view.php?id=' + e.id;
    } else if (e.type == 'course_section') {
      return '/course/view.php?id=' + e.course_id;
      // return '/course/section.php?id=' + e.id;
      return '/course/section.php?id=' + e.id;
    } else if (e.type == 'module') {
      return '/mod/'+e.module_table+'/view.php?id=' + e.id;
    }
    
    return '';
  };

  return module;
})(globalThis.TagSearch || {});
