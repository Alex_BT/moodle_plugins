<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');

global $OUTPUT, $PAGE, $CFG;

require_login();

$PAGE->set_title('Поиск по тегам');
$PAGE->set_heading('Поиск по тегам');
$PAGE->set_pagelayout('frontpage');

echo $OUTPUT->header();
?>

<style>
 .tag-line {
     display: flex;
     gap: 15px;
 }

 .tag-line__operator {
     min-width: 150px;
 }

 .tag-line__values {
     flex: 1;
 }

 .tag-line__inputs {
     text-align: right;
 }
</style>

<div class="container" id="SearchContainer">

    <div class="form-group">
        <label for="">Строка поиска тега</label>
        <input id="TagSearchInput" name="operator_main_value" type="text" value="" class="form-control" />
    </div>

    <ul id="TagSearchResult" class=""list-group">
        
    </ul>

    <h5>Найденные результаты</h5>

    <ul class="list-group" id="EntitySearchResult"></ul>
</div>

<script src="/local/custompages/script/TagSearch.js?v=001"></script>

<script type="module">
 require(['format_complexhierarchy/moodle'], function(moodle){
     globalThis.TagSearch.Init(moodle, SearchContainer);
  });
</script>

<?php
echo $OUTPUT->footer();
